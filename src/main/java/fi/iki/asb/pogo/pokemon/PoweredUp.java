package fi.iki.asb.pogo.pokemon;

public enum PoweredUp {
	MAYBE("Maybe", true),
	YES("Yes", true),
	NO("No", false);

	private final String label;
	private final boolean isPoweredUp;

	private PoweredUp(String label, boolean isPoweredUp) {
		this.label = label;
		this.isPoweredUp = isPoweredUp;
	}

	@Override
	public String toString() {
		return label;
	}

	public boolean isPoweredUp() {
		return isPoweredUp;
	}

	public static PoweredUp fromLabel(String label) {
		for (PoweredUp pu: values()) {
			if (pu.toString().equals(label)) {
				return pu;
			}
		}

		return PoweredUp.MAYBE;
	}
}
