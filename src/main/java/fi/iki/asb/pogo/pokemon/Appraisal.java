package fi.iki.asb.pogo.pokemon;

/**
 * A pokemon appraisal.
 */
public final class Appraisal {

	private boolean staminaBest;
	private boolean attackBest;
	private boolean defenseBest;
	private TopStatTier topTier;
	private PerfectionTier perfectionTier;

	private Appraisal(
			boolean staminaBest,
			boolean attackBest,
			boolean defenseBest,
			TopStatTier topTier,
			PerfectionTier perfectionTier) {
		this.staminaBest = staminaBest;
		this.attackBest = attackBest;
		this.defenseBest = defenseBest;
		this.topTier = topTier;
		this.perfectionTier = perfectionTier;
	}

	public static Appraisal unknown() {
		return new Appraisal(
				false,
				false,
				false,
				TopStatTier.UNKNOWN,
				PerfectionTier.UNKNOWN);
	}

	public static Appraisal from(Appraisal original) {
		return new Appraisal(
				original.isStaminaBest(),
				original.isAttackBest(),
				original.isDefenseBest(),
				original.getTopStatTier(),
				original.getPerfectionTier());
	}

	public boolean isStaminaBest() {
		return staminaBest;
	}

	public void setStaminaBest(boolean staminaBest) {
		this.staminaBest = staminaBest;
	}

	public boolean isAttackBest() {
		return attackBest;
	}

	public void setAttackBest(boolean attackBest) {
		this.attackBest = attackBest;
	}

	public boolean isDefenseBest() {
		return defenseBest;
	}

	public void setDefenseBest(boolean defenseBest) {
		this.defenseBest = defenseBest;
	}
	
	public TopStatTier getTopStatTier() {
		return topTier;
	}

	public void setTopStatTier(TopStatTier topTier) {
		this.topTier = topTier;
	}
	
	public PerfectionTier getPerfectionTier() {
		return perfectionTier;
	}
	
	public void setPerfectionTier(PerfectionTier perfectionTier) {
		this.perfectionTier = perfectionTier;
	}

	public boolean isEmpty() {
		return ! (staminaBest || attackBest || defenseBest); 
	}

	/**
	 * Do the individual values match this appraisal?
	 */
	public boolean matches(IndividualValues ivs) {
		if (! ivs.isComplete()) {
			return false;
		}

		int sta = ivs.getStamina();
		int att = ivs.getAttack();
		int def = ivs.getDefense();

		if (isStaminaBest() && isAttackBest() && sta != att)
			return false;

		if (isStaminaBest() && isDefenseBest() && sta != def)
			return false;

		if (isAttackBest() && isDefenseBest() && att != def)
			return false;

		if (isStaminaBest()) {
			if (sta <= att && ! isAttackBest())
				return false;

			if (sta <= def && ! isDefenseBest())
				return false;

			if (isAttackBest() && sta != att)
				return false;

			if (isDefenseBest() && sta != def)
				return false;
		}

		if (isAttackBest()) {
			if (att <= sta && ! isStaminaBest())
				return false;

			if (att <= def && ! isDefenseBest())
				return false;
		}

		if (isDefenseBest()) {
			if (def <= sta && ! isStaminaBest())
				return false;

			if (def <= att && ! isAttackBest())
				return false;
		}

		// Get top stat value and compare to appraisal.
		final int topStat = Math.max(Math.max(att, def), sta);
		if (! getTopStatTier().matches(topStat))
			return false;

		return perfectionTier.matches(ivs.getPerfection());
	}
	
	// =================================================================== //

	/**
	 * Appraisal top stat tier.
	 */
	public enum TopStatTier {
		UNKNOWN("Unknown", 15, 0),
		TIER_1("15", 15, 15),
		TIER_2("13 - 14", 14, 13),
		TIER_3("8 - 12", 12, 8),
		TIER_4("0 - 7", 7, 0);

		private final String label;
		private final int max;
		private final int min;

		private TopStatTier(String label, int max, int min) {
			this.label = label;
			this.max = max;
			this.min = min;
		}

		@Override
		public String toString() {
			return label;
		}

		public boolean matches(Integer value) {
			return value != null
					&& value >= min
					&& value <= max;
		}

		public static TopStatTier fromLabel(String label) {
			if (label == null) {
				return null;
			}

			for (TopStatTier tt: values()) {
				if (tt.toString().equals(label)) {
					return tt;
				}
			}

			throw new IllegalArgumentException("Label [" + label + "]");
		}

		public static TopStatTier from(Integer iv) {
			if (TIER_1.matches(iv)) return TIER_1;
			if (TIER_2.matches(iv)) return TIER_2;
			if (TIER_3.matches(iv)) return TIER_3;
			if (TIER_4.matches(iv)) return TIER_4;
			else return UNKNOWN;
		}
	}

	/**
	 * Appraisal perfection tier.
	 */
	public enum PerfectionTier {
		UNKNOWN("Unknown",  100.1, -0.1),
		TIER_1("82.2% - 100%", 100.1, 82.1),
		TIER_2("66.7% - 80%", 80.1, 66.6),
		TIER_3("51.1% - 64.4%", 64.5, 51.0),
		TIER_4("0% - 48.9%", 49.0, -0.1);

		private final String label;
		private final double max;
		private final double min;

		private PerfectionTier(String label, double max, double min) {
			this.label = label;
			this.max = max;
			this.min = min;
		}

		@Override
		public String toString() {
			return label;
		}

		public boolean matches(Double value) {
			return value != null
					&& value >= min
					&& value <= max;
		}

		public static PerfectionTier fromLabel(String label) {
			if (label == null) {
				return null;
			}

			for (PerfectionTier pt: values()) {
				if (pt.toString().equals(label)) {
					return pt;
				}
			}
			
			throw new IllegalArgumentException("Label [" + label + "]");
		}

		public static PerfectionTier from(Double perfection) {
			if (TIER_1.matches(perfection)) return TIER_1;
			if (TIER_2.matches(perfection)) return TIER_2;
			if (TIER_3.matches(perfection)) return TIER_3;
			if (TIER_4.matches(perfection)) return TIER_4;
			else return UNKNOWN;
		}
	}

	@Override
	public String toString() {
		return "Appraisal [staminaBest=" + staminaBest
				+ ", attackBest=" + attackBest
				+ ", defenseBest=" + defenseBest
				+ ", topTier=\"" + topTier
				+ "\", perfectionTier=\"" + perfectionTier + "\"]";
	}
}
