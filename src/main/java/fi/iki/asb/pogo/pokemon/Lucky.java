package fi.iki.asb.pogo.pokemon;

public enum Lucky {
	YES("Yes", true),
	NO("No", false);

	private final String label;
	private final boolean isLucky;

	private Lucky(String label, boolean isLucky) {
		this.label = label;
		this.isLucky = isLucky;
	}

	@Override
	public String toString() {
		return label;
	}

	public boolean isLucky() {
		return isLucky;
	}
	
	public static Lucky fromLabel(String label) {
		for (Lucky pu: values()) {
			if (pu.toString().equals(label)) {
				return pu;
			}
		}

		return NO;
	}
}