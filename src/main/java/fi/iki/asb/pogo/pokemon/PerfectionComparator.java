package fi.iki.asb.pogo.pokemon;
import java.util.Comparator;

public final class PerfectionComparator implements Comparator<Pokemon> {

	private static final PerfectionComparator INSTANCE = new PerfectionComparator();

	private PerfectionComparator() {
		// NOP
	}

	public static PerfectionComparator instance() {
		return INSTANCE;
	}

	@Override
	public int compare(Pokemon o1, Pokemon o2) {
		IndividualValues iv1 = o1.getIndividualValues();
		IndividualValues iv2 = o2.getIndividualValues();

		int perf = Double.compare(iv2.getPerfection(), iv1.getPerfection());
		if (perf != 0) {
			return perf;
		}

		int att = Double.compare(iv2.getAttack(), iv1.getAttack());
		if (att != 0) {
			return att;
		}

		int def = Double.compare(iv2.getDefense(), iv1.getDefense());
		if (def != 0) {
			return def;
		}

		return Double.compare(iv2.getStamina(), iv1.getStamina());
	}
}
