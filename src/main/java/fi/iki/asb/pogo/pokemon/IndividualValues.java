package fi.iki.asb.pogo.pokemon;

import static fi.iki.asb.pogo.util.Format.toStr;

public final class IndividualValues {

	private Integer stamina;
	private Integer attack;
	private Integer defense;

	private IndividualValues(Integer stamina, Integer attack, Integer defense) {
		setSAD(stamina, attack, defense);
	}

	public static IndividualValues unknown() {
		return new IndividualValues(null, null, null);
	}
	
	public static IndividualValues from(IndividualValues original) {
		return new IndividualValues(original.getStamina(), original.getAttack(), original.getDefense());
	}
	
	public void setSAD(Integer sta, Integer att, Integer def) {
		setStamina(sta);
		setAttack(att);
		setDefense(def);
	}

	public Integer getStamina() {
		return stamina;
	}

	public void setStamina(Integer stamina) {
		validate(stamina);
		this.stamina = stamina;
	}

	public Integer getAttack() {
		return attack;
	}

	public void setAttack(Integer attack) {
		validate(attack);
		this.attack = attack;
	}

	public Integer getDefense() {
		return defense;
	}

	public void setDefense(Integer defense) {
		validate(defense);
		this.defense = defense;
	}

	public void clear() {
		setSAD(null, null, null);
	}
	
	@Override
	public String toString() {
		return String.format("IndividualValues [stamina=%s, attack=%s, defense=%s, perfection=%s]",
				toStr(getStamina()),
				toStr(getAttack()),
				toStr(getDefense()),
				toStr(getPerfection(), 1));
	}

	public Double getPerfection() {
		if (isComplete()) {
			return (getStamina() + getAttack() + getDefense()) / 0.45;
		} else {
			return null;
		}
	}

	public boolean isComplete() {
		return getStamina() != null && getAttack() != null && getDefense() != null;
	}

	private void validate(Integer iv) {
		if (iv == null) {
			return;
		}

		if (iv < 0 || iv > 15) {
			throw new IllegalArgumentException("Illegal IV [" + iv + "]");
		}
	}
}
