package fi.iki.asb.pogo.pokemon;

import fi.iki.asb.pogo.calculator.Level;
import fi.iki.asb.pogo.pokedex.PokemonSpecies;

public final class Pokemon {

	private final PokemonSpecies species;
	private final IndividualValues ivs;
	private Integer powerUpCost;
	private PoweredUp poweredUp;
	private Lucky lucky;
	private Level level;
	private Integer cp;
	private Integer hp;
	private Appraisal appraisal;

	// ================================================================== //

	private Pokemon(
			PokemonSpecies species,
			IndividualValues ivs,
			Integer powerUpCost,
			PoweredUp poweredUp,
			Lucky lucky,
			Level level,
			Integer cp,
			Integer hp,
			Appraisal appraisal) {
		this.species = species;
		this.ivs = ivs;
		this.powerUpCost = powerUpCost;
		this.poweredUp = poweredUp;
		this.lucky = lucky;
		this.level = level;
		this.cp = cp;
		this.hp = hp;
		this.appraisal = appraisal;
	}

	// ================================================================== //

	public static Pokemon ofSpecies(PokemonSpecies species) {
		return new Pokemon(
				species,
				IndividualValues.unknown(),
				null,
				PoweredUp.MAYBE,
				Lucky.NO,
				null,
				null,
				null,
				Appraisal.unknown());
	}

	public static Pokemon from(Pokemon original) {
		return new Pokemon(
				original.getSpecies(),
				IndividualValues.from(original.getIndividualValues()),
				original.getPowerUpCost(),
				original.isPoweredUp(),
				original.isLucky(),
				original.getLevel(),
				original.getCp(),
				original.getHp(),
				Appraisal.from(original.getAppraisal()));
	}

	// ================================================================== //

	public PokemonSpecies getSpecies() {
		return species;
	}

	public IndividualValues getIndividualValues() {
		return ivs;
	}
	
	public Integer getPowerUpCost() {
		return powerUpCost;
	}
	
	public void setPowerUpCost(Integer powerUpCost) {
		this.powerUpCost = powerUpCost;
	}

	public PoweredUp isPoweredUp() {
		return poweredUp;
	}
	
	public void setPoweredUp(PoweredUp poweredUp) {
		this.poweredUp = poweredUp;
	}
	
	public Lucky isLucky() {
		return lucky;
	}
	
	public void setLucky(Lucky lucky) {
		this.lucky = lucky;
	}
	
	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public Integer getCp() {
		return cp;
	}

	public void setCp(Integer cp) {
		this.cp = cp;
	}

	public Integer getHp() {
		return hp;
	}

	public void setHp(Integer hp) {
		this.hp = hp;
	}
	
	public Appraisal getAppraisal() {
		return appraisal;
	}

	@Override
	public String toString() {
		return "Pokemon [species=" + species.toComplexString()
				+ ", cp=" + cp
				+ ", hp=" + hp
				+ ", ivs=" + ivs
				+ ", powerUpCost=" + powerUpCost
				+ ", poweredUp=" + poweredUp
				+ ", lucky=" + lucky
				+ ", level=" + level
				+ ", appraisal=" + appraisal + "]";
	}

	public Pokemon evolve(PokemonSpecies evolvedForm) {
		if (getSpecies().getEvolutions().contains(evolvedForm.getName()) == false
				&& getSpecies().getPredecessor().equals(evolvedForm.getName()) == false) {
			throw new IllegalArgumentException("Can not evolve " + getSpecies().getName() + " to " + evolvedForm.getName());
		}

		return new Pokemon(
				evolvedForm,
				IndividualValues.from(getIndividualValues()),
				getPowerUpCost(),
				isPoweredUp(),
				isLucky(),
				getLevel(),
				null,
				null,
				Appraisal.from(getAppraisal()));
	}
}
