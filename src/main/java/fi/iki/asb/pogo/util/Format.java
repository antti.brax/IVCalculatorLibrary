package fi.iki.asb.pogo.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class Format {
	
	private Format() {
	}

	public static String toStr(Integer v) {
		return String.valueOf(v);
	}

	public static String toStr(Double v, int decimals) {
		if (v == null) {
			return null;
		} else {
		    BigDecimal bd = new BigDecimal(v);
		    bd = bd.setScale(decimals, RoundingMode.HALF_UP);
		    return bd.toString();
		}
	}

}
