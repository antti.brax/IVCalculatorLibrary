package fi.iki.asb.pogo.util;

public interface Predicate<T> {

	boolean test(T t);

}
