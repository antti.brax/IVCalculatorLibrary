package fi.iki.asb.pogo.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

public final class Json {

	private Json() {
		// NOP
	}

	public static <T> T loadJson(String resource, Class<T> classOfT) {
		try {
			InputStream stream = Json.class.getResourceAsStream(resource);
			Reader reader = new InputStreamReader(stream, "UTF-8");
			Gson gson = new Gson();
			return gson.fromJson(reader, classOfT);
		} catch (JsonIOException | UnsupportedEncodingException ex) {
			throw new RuntimeException(ex);
		}
	}

}
