package fi.iki.asb.pogo.pokedex;

public final class Stats {

	private Integer stamina;
	private Integer attack;
	private Integer defense;
	
	public Stats() {
	}
	
	Stats(int stamina, int attack, int defense) {
		this.stamina = stamina;
		this.attack = attack;
		this.defense = defense;
	}

	public Integer getStamina() {
		return stamina;
	}

	public Integer getAttack() {
		return attack;
	}

	public Integer getDefense() {
		return defense;
	}

	void setAttack(int attack) {
		this.attack = attack;
	}
	
	void setDefense(int defense) {
		this.defense = defense;
	}
	
	void setStamina(int stamina) {
		this.stamina = stamina;
	}

	@Override
	public String toString() {
		return "Stats [stamina=" + stamina + ", attack=" + attack + ", defense=" + defense + "]";
	}
}
