package fi.iki.asb.pogo.pokedex;

public class Move {
	
	private String id;
	private String name;
	private double power;
	private int duration;
	private int energyDelta;
	private String type;

	Move(String id,
			String name,
			double power,
			int duration,
			int energyDelta,
			String type) {
		super();
		this.id = id;
		this.name = name;
		this.power = power;
		this.duration = duration;
		this.energyDelta = energyDelta;
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public double getPower() {
		return power;
	}
	public int getDuration() {
		return duration;
	}
	public int getEnergyDelta() {
		return Math.abs(energyDelta);
	}
	public String getType() {
		return type;
	}
}
