package fi.iki.asb.pogo.pokedex;

import static fi.iki.asb.pogo.util.Json.loadJson;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Moves {

	private List<Move> allMoves;

	// ================================================================== //

	private Moves() {
		allMoves = Arrays.asList(loadJson("/fi/iki/asb/pogo/moves.json", Move[].class));
	}

	// ================================================================== //

	public static Moves instance() {
		return new Moves();
	}

	// ================================================================== //

	public List<Move> getAllMoves() {
		return Collections.unmodifiableList(allMoves);
	}

	public Move findById(String id) {
		for (Move m: allMoves) {
			if (m.getId().equals(id)) {
				return m;
			}
		}

		return null;
	}

}
