package fi.iki.asb.pogo.pokedex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fi.iki.asb.pogo.util.Predicate;

import static fi.iki.asb.pogo.util.Json.loadJson;

public class Pokedex {

	private List<PokemonSpecies> pokemonSpecies;

	// ================================================================== //

	private Pokedex() {
		pokemonSpecies = Arrays.asList(loadJson("/fi/iki/asb/pogo/pokedex.json", PokemonSpecies[].class));
		for (PokemonSpecies p: pokemonSpecies) {
			p.setName(p.getName().toUpperCase().trim());
		}
	}

	// ================================================================== //

	public static Pokedex instance() {
		return new Pokedex();
	}

	// ================================================================== //

	/**
	 * Find species matching number.
	 */
	public List<PokemonSpecies> findByNumber(final int number) {
		final Predicate<PokemonSpecies> p = new Predicate<PokemonSpecies>() {
			@Override
			public boolean test(PokemonSpecies t) {
				return t.getPokedexNumber() == number;
			}
		};

		return find(p);
	}

	/**
	 * Find species matching name.
	 */
	public PokemonSpecies findByName(final String name) {
		final String nameUpperCase = name.toUpperCase();
		final Predicate<PokemonSpecies> p = new Predicate<PokemonSpecies>() {
			@Override
			public boolean test(PokemonSpecies t) {
				return t.getName().equals(nameUpperCase);
			}
		};
		
		List<PokemonSpecies> results = find(p);
		if (results.isEmpty()) {
			return null;
		} else {
			return results.get(0);
		}
	}

	public List<PokemonSpecies> find(Predicate<PokemonSpecies> p) {
		final List<PokemonSpecies> matches = new ArrayList<>();
		for (PokemonSpecies species: pokemonSpecies) {
			if (p.test(species)) {
				matches.add(species);
			}
		}

		return matches;
	}
}
