package fi.iki.asb.pogo.pokedex;

import java.util.ArrayList;
import java.util.List;

public class PokemonSpecies {

	private int pokedexNumber;
	private String name;
	private Stats stats;
	private List<String> evolutions;
	private String predecessor;
	private List<String> fastMoves;
	private List<String> chargeMoves;
	private List<String> types;

	PokemonSpecies(
			int pokedexNumber,
			String name,
			Stats stats,
			List<String> evolutions,
			String predecessor,
			List<String> fastMoves,
			List<String> chargeMoves,
			List<String> types) {
		this.pokedexNumber = pokedexNumber;
		this.name = name;
		this.stats = stats;
		this.evolutions = new ArrayList<>(evolutions);
		this.predecessor = predecessor;
		this.fastMoves = fastMoves;
		this.chargeMoves = chargeMoves;
		this.types = types;
	}

	public int getPokedexNumber() {
		return pokedexNumber;
	}

	public String getName() {
		return name;
	}
	
	void setName(String name) {
		this.name = name;
	}

	public Stats getStats() {
		return stats;
	}

	public List<String> getEvolutions() {
		return evolutions;
	}

	public String getPredecessor() {
		return predecessor;
	}

	void setPredecessor(String predecessor) {
		this.predecessor = predecessor;
	}

	public List<String> getFastMoves() {
		return fastMoves;
	}
	
	void setFastMoves(List<String> fastMoves) {
		this.fastMoves = fastMoves;
	}

	public List<String> getChargeMoves() {
		return chargeMoves;
	}
	
	void setChargeMoves(List<String> chargeMoves) {
		this.chargeMoves = chargeMoves;
	}
	
	public List<String> getTypes() {
		return types;
	}
	
	void setTypes(List<String> types) {
		this.types = types;
	}
	
	@Override
	public String toString() {
		return name;
	}

	public String toComplexString() {
		return "PokemonSpecies [name=" + name
				+ ", pokedexNumber=" + pokedexNumber
				+ ", stats=" + stats
				+ ", evolutions=" + evolutions
				+ ", predecessor=" + predecessor + "]";
	}
}
