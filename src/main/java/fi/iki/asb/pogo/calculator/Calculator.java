package fi.iki.asb.pogo.calculator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import fi.iki.asb.pogo.pokedex.Stats;
import fi.iki.asb.pogo.pokemon.Appraisal.PerfectionTier;
import fi.iki.asb.pogo.pokemon.Appraisal.TopStatTier;
import fi.iki.asb.pogo.pokemon.Appraisal;
import fi.iki.asb.pogo.pokemon.IndividualValues;
import fi.iki.asb.pogo.pokemon.PerfectionComparator;
import fi.iki.asb.pogo.pokemon.Pokemon;

/**
 * IV Calculator. Class is immutable and thread safe.
 */
public final class Calculator {

	private final Levels levels = Levels.instance();

	// ================================================================== //

	private Calculator() {
	}

	// ================================================================== //

	public static Calculator instance() {
		return new Calculator();
	}

	// ================================================================== //

	/**
	 * Find pokemon IV and level combinations that match the provided
	 * values. The fields that are taken into account are:
	 * 
	 * <ul>
	 *   <li>CP (required)</li>
	 *   <li>HP (required)</li>
	 *   <li>Level</li>
	 *   <li>Powered up (true/false)</li>
	 *   <li>Power up cost</li>
	 *   <li>Appraisal top stat</li>
	 *   <li>Appraisal top stat tier</li>
	 *   <li>Appraisal perfection tier</li>
	 * </ul>
	 *
	 * @return A collection of valid pokemons.
	 */
	public List<Pokemon> calculateIV(Pokemon pokemon) {
		final ArrayList<Pokemon> matches = new ArrayList<>();
		PokemonCollector collector = new PokemonCollector() {
			@Override
			public void start() {
				// NOP
			}
			
			@Override
			public void add(Pokemon pokemon) {
				matches.add(pokemon);
			}
			
			@Override
			public void end(boolean interrupted) {
				// NOP
			}
		};
		
		calculateIV(pokemon, collector);
		Collections.sort(matches, PerfectionComparator.instance());
		return matches;
	}

	/**
	 * Find pokemon IV and level combinations that match the provided
	 * values as defined in <code>calculateIV(Pokemon)</code>
	 *
	 * <p>The calculation stops when all matching pokemon have been
	 * found or when the collector throws an {@link InterruptedException}
	 * from the add-method.</p>
	 *
	 * @see Calculator#calculateIv(Pokemon)
	 * @param pokemon The pokemon being evaluated.
	 * @param collector The collector that collects the matches.
	 */
	public void calculateIV(
			final Pokemon pokemon,
			final PokemonCollector collector) {

		boolean interrupted = true;

		collector.start();

		if (pokemon.getCp() != null && pokemon.getHp() != null) {
			try {
				findLevels(Pokemon.from(pokemon), collector);
				interrupted = false;
			} catch (InterruptedException e) {
				// NOP
			}
		}

		collector.end(interrupted);
	}

	/**
	 * Calculate HP for the pokemon and store it in it's HP attribute.
	 * 
	 * @return <code>true</code> if HP was calculated.
	 */
	public boolean calculateHp(Pokemon pokemon) {
		if (pokemon.getSpecies() == null) {
			return false;
		}

		if (pokemon.getIndividualValues().getStamina() == null) {
			return false;
		}

		if (pokemon.getLevel() == null) {
			return false;
		}

		pokemon.setHp(getHP(pokemon));
		return true;
	}

	/**
	 * Calculate CP for the pokemon and store it in it's CP attribute.
	 * 
	 * @return <code>true</code> if HP was calculated.
	 */
	public boolean calculateCp(Pokemon pokemon) {
		final IndividualValues ivs = pokemon.getIndividualValues();
		if (ivs.getStamina() == null) {
			return false;
		}
		if (ivs.getAttack() == null) {
			return false;
		}
		if (ivs.getDefense() == null) {
			return false;
		}
		if (pokemon.getLevel() == null) {
			return false;
		}
		if (pokemon.getSpecies() == null) {
			return false;
		}

		pokemon.setCp(getCP(pokemon));
		return true;
	}

	public boolean calculateAppraisal(Pokemon pokemon) {
		final IndividualValues ivs = pokemon.getIndividualValues();
		if (ivs.getStamina() == null) {
			return false;
		}
		if (ivs.getAttack() == null) {
			return false;
		}
		if (ivs.getDefense() == null) {
			return false;
		}

		int sta = ivs.getStamina();
		int att = ivs.getAttack();
		int def = ivs.getDefense();
		double perf = ivs.getPerfection();
		int best = -1;
		
		pokemon.getAppraisal().setStaminaBest(false);
		pokemon.getAppraisal().setAttackBest(false);
		pokemon.getAppraisal().setDefenseBest(false);
		pokemon.getAppraisal().setTopStatTier(TopStatTier.UNKNOWN);
		pokemon.getAppraisal().setPerfectionTier(PerfectionTier.UNKNOWN);

		if (sta >= att && sta >= def) {
			pokemon.getAppraisal().setStaminaBest(true);
			best = sta;
		}

		if (att >= sta && att >= def) {
			pokemon.getAppraisal().setAttackBest(true);
			best = att;
		}

		if (def >= sta && def >= att) {
			pokemon.getAppraisal().setDefenseBest(true);
			best = def;
		}

		pokemon.getAppraisal().setTopStatTier(TopStatTier.from(best));
		pokemon.getAppraisal().setPerfectionTier(PerfectionTier.from(perf));
		return true;
	}

	// ================================================================== //

	/**
	 * For a <code>pokemon</code> that has CP and HP defined, go through each
	 * level, stamina, attack and defense value and return the combinations that
	 * are valid.
	 *
	 * @return A collection of valid pokemons.
	 */
	private void findLevels(
			final Pokemon candidate,
			final PokemonCollector collector)
					throws InterruptedException {

		// Do not overwrite level.
		if (candidate.getLevel() != null) {
			findStamina(candidate, collector);
			return;
		}

		for (Level level : getPossibleLevels(candidate)) {
			candidate.setLevel(level);
			candidate.setPowerUpCost(level.getPowerUpCost(candidate.isLucky()));
			findStamina(candidate, collector);
		}
	}

	/**
	 * For a <code>pokemon</code> that has CP, HP and level defined, go through
	 * each stamina, attack and defense value and return the combinations that
	 * are valid.
	 * 
	 * @return A collection of valid pokemons.
	 */
	private void findStamina(
			final Pokemon candidate,
			final PokemonCollector collector)
					throws InterruptedException {

		// Do not overwrite known stamina.
		if (candidate.getIndividualValues().getStamina() != null) {
			findAttack(candidate, collector);
			return;
		}

		final int hp = candidate.getHp();

		for (int stamina = 15; stamina >= 0; stamina--) {
			candidate.getIndividualValues().setStamina(stamina);
			if (hp == getHP(candidate)) {
				findAttack(candidate, collector);
			}
			candidate.getIndividualValues().setStamina(null);
		}
	}

	/**
	 * For a <code>pokemon</code> that has CP, HP, level and stamina defined, go
	 * through each attack and defense value and return the combinations that
	 * are valid.
	 * 
	 * @return A collection of valid pokemons.
	 */
	private void findAttack(
			final Pokemon candidate,
			final PokemonCollector collector)
					throws InterruptedException {

		// Do not overwrite known attack.
		if (candidate.getIndividualValues().getAttack() != null) {
			findDefense(candidate, collector);
			return;
		}

		for (int attack = 15; attack >= 0; attack--) {
			candidate.getIndividualValues().setAttack(attack);
			findDefense(candidate, collector);
			candidate.getIndividualValues().setAttack(null);
		}
	}

	/**
	 * For a <code>pokemon</code> that has CP, HP, level, stamina and attack
	 * defined, go through each defense value and return the combinations that
	 * are valid.
	 * 
	 * @return A collection of valid pokemons.
	 */
	private void findDefense(
			final Pokemon candidate,
			final PokemonCollector collector)
					throws InterruptedException {

		// Do not overwrite known defense.
		if (candidate.getIndividualValues().getDefense() != null) {
			validate(candidate, collector);
			return;
		}

		for (int defense = 15; defense >= 0; defense--) {
			candidate.getIndividualValues().setDefense(defense);
			validate(candidate, collector);
			candidate.getIndividualValues().setDefense(null);
		}
	}

	/**
	 * Check if pokemon's stats and level match the CP and the IV's match
	 * the appraisal.
	 *
	 * @return If <code>pokemon</code> is valid, a singleton collection
	 *         containing it is returned. Otherwise an empty collection is
	 *         returned.
	 */
	private void validate(
			final Pokemon candidate,
			final PokemonCollector collector)
					throws InterruptedException {
		if (candidate.getCp() != getCP(candidate))
			return;

		Appraisal appraisal = candidate.getAppraisal();
		if (!appraisal.matches(candidate.getIndividualValues())) {
			return;
		}
		
		Pokemon result = Pokemon.from(candidate);
		if (result.getAppraisal().isEmpty()) {
			calculateAppraisal(result);
		}

		collector.add(result);
	}

	/**
	 * Calculate CP based on <code>pokemon</code>'s species, individual values
	 * and level.
	 */
	private int getCP(Pokemon pokemon) {
		final Stats baseStats = pokemon.getSpecies().getStats();
		final IndividualValues ivs = pokemon.getIndividualValues();
		final Double cpMultiplier = pokemon.getLevel().getCpMultiplier();

		return (int) Math.max(10,
				Math.floor((baseStats.getAttack() + ivs.getAttack())
						* Math.pow(baseStats.getDefense() + ivs.getDefense(), 0.5)
						* Math.pow(baseStats.getStamina() + ivs.getStamina(), 0.5) * Math.pow(cpMultiplier, 2) / 10));
	}

	/**
	 * Calculate the HP given the IV stamina and ECpM
	 */
	private int getHP(Pokemon pokemon) {
		final double cpMultiplier = pokemon.getLevel().getCpMultiplier();
		final int baseStamina = pokemon.getSpecies().getStats().getStamina();
		final int ivStamina = pokemon.getIndividualValues().getStamina();

		return (int) Math.max(10, Math.floor(cpMultiplier * (baseStamina + ivStamina)));
	}

	/**
	 * Get all possible levels for the pokemon.
	 */
	Collection<Level> getPossibleLevels(Pokemon pokemon) {
		final List<Level> allLevels = levels.getAllLevels();
		final Collection<Level> matches = new ArrayList<>();
		final boolean lucky = pokemon.isLucky().isLucky();
		final boolean notPowered = ! pokemon.isPoweredUp().isPoweredUp();

		for (Level level: allLevels) {
			if (notPowered && level.isHalfLevel()) {
				continue;
			}

			int levelPowerUpCost = level.getPowerUpCost(pokemon.isLucky());
			if (lucky) {
				levelPowerUpCost = levelPowerUpCost / 2;
			}

			if (pokemon.getPowerUpCost() == null || 
					levelPowerUpCost == pokemon.getPowerUpCost().intValue()) {
				matches.add(level);
			}
		}
		
		return matches;
	}
}
