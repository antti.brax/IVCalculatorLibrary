package fi.iki.asb.pogo.calculator;

import fi.iki.asb.pogo.pokemon.Lucky;

public class Level {

	private String name;
	private int powerUpCost;
	private double cpMultiplier;

	/**
	 * The level name.
	 */
	public String getName() {
		return name;
	}

	public int getPowerUpCost(Lucky lucky) {
		if (lucky.isLucky()) {
			return powerUpCost / 2;
		} else {
			return powerUpCost;
		}
	}

	public double getCpMultiplier() {
		return cpMultiplier;
	}
	
	public boolean isHalfLevel() {
		return getName().endsWith(".5");
	}

	public int getBaseLevel() {
		return (int) Double.parseDouble(getName());
	}

	@Override
	public String toString() {
		return "Level [name=" + name
				+ ", powerUpCost=" + powerUpCost
				+ ", cpMultiplier=" + cpMultiplier + "]";
	}
}
