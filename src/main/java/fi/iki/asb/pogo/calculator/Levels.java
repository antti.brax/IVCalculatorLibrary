package fi.iki.asb.pogo.calculator;

import static fi.iki.asb.pogo.util.Json.loadJson;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import fi.iki.asb.pogo.pokemon.Lucky;
import fi.iki.asb.pogo.pokemon.PoweredUp;

public final class Levels {

	private List<Level> allLevels;

	// ================================================================== //

	private Levels() {
		allLevels = Arrays.asList(loadJson("/fi/iki/asb/pogo/level.json", Level[].class));
	}

	// ================================================================== //

	public static Levels instance() {
		return new Levels();
	}

	// ================================================================== //

	public List<Level> getAllLevels() {
		return Collections.unmodifiableList(allLevels);
	}
	
	public Level getLevel(String name) {
		if (name == null)
			return null;

		for (Level level: allLevels) {
			if (level.getName().equals(name)) {
				return level;
			}
		}

		throw new IllegalArgumentException("Level [" + name + "]");
	}

	public Level getLevel(int baseLevel) {
		for (Level level: allLevels) {
			if (level.getBaseLevel() == baseLevel) {
				return level;
			}
		}

		return null;
	}

	public Level getMaxLevel(int powerUpCost, PoweredUp poweredUp, Lucky lucky) {
		for (int i = allLevels.size() - 1; i >= 0; i--) {
			Level level = allLevels.get(i);
			if (level.isHalfLevel() && ! poweredUp.isPoweredUp())
				continue;

			if (allLevels.get(i).getPowerUpCost(lucky) == powerUpCost)
				return allLevels.get(i);
		}
		
		return null;
	}

}
