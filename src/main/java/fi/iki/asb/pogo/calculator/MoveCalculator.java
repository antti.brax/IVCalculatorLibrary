package fi.iki.asb.pogo.calculator;

import fi.iki.asb.pogo.pokedex.Move;
import fi.iki.asb.pogo.pokemon.Pokemon;

public class MoveCalculator {

	private MoveCalculator() {
	}

	// ================================================================== //

	public static MoveCalculator instance() {
		return new MoveCalculator();
	}

	// ================================================================== //

	/**
	 * Power per second for fast and charge move.
	 * 
	 * How many of the move's power points can one utilize in a second on
	 * average.
	 */
	public Double calculatePPS(
			Pokemon pokemon,
			Move fastMove,
			Move chargeMove) {

		if (fastMove.getEnergyDelta() == 0) {
			return 0.0;
		}

		final double chargePower = calculatePower(pokemon, chargeMove);
		final double chargeEnergy = chargeMove.getEnergyDelta();
		final double chargeDuration = chargeMove.getDuration();

		final double fastPower = calculatePower(pokemon, fastMove);
		final double fastEnergy = fastMove.getEnergyDelta();
		final double fastDuration = fastMove.getDuration();

		double fastCount;
		double chargeCount;

		if (chargeMove.getEnergyDelta() == 100) {
			// With one bar moves the excess energy is lost.
			fastCount = Math.ceil(chargeEnergy / fastEnergy);
			chargeCount = 1;
		} else {
			// If we perform 1000 fast moves in a row and throw a charge move
			// as soon as it is possible, how much power per second could we
			// utilise? This takes into account the energy that is left over
			// when the charge move is thrown.
			fastCount = 1000.0;
			chargeCount = ( fastCount * fastEnergy ) / chargeEnergy;
		}

		final double totalPower = fastCount * fastPower + chargeCount * chargePower;
		final double totalDuration = fastCount * fastDuration + chargeCount * chargeDuration;
		return totalPower / (totalDuration / 1000.0);
	}

	/**
	 * Power per second for one move.
	 * 
	 * How many of the move's power points can one utilize in a second on
	 * average.
	 */
	public double calculatePPS(Pokemon pokemon, Move move) {
		double power = calculatePower(pokemon, move);
		double duration = move.getDuration() / 1000.0;
		return power / duration;
	}

	private double calculatePower(Pokemon pokemon, Move move) {
		double mul = pokemon.getSpecies().getTypes().contains(move.getType()) ? 1.2 : 1.0;
		return move.getPower() * mul;
	}
}
