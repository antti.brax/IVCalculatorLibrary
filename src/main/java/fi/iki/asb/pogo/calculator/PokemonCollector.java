package fi.iki.asb.pogo.calculator;

import fi.iki.asb.pogo.pokemon.Pokemon;

public interface PokemonCollector {

	void start();

	/**
	 * Add a pokemon to this collector.
	 *
	 * @param pokemon The pokemon.
	 * @throws InterruptedException If the collector can not handle any more
	 * 		pokemon.
	 */
	void add(Pokemon pokemon) throws InterruptedException;

	void end(boolean interrupted);

}
