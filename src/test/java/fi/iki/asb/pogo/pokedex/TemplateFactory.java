package fi.iki.asb.pogo.pokedex;

import com.google.gson.JsonObject;

public class TemplateFactory {

	private static String POKEMON_TEMPLATE_PATTERN = "^V(\\d\\d\\d\\d)_POKEMON_.*$";
	private static String MOVE_TEMPLATE_PATTERN = "^V(\\d\\d\\d\\d)_MOVE_.*$";

	public static Template getTemplate(JsonObject json) {
		String templateId = json.get("templateId").getAsString();
		
		if (templateId.equals("V0230_MOVE_WATER_GUN_FAST")) {

			System.err.println("X");
		}
		
		if (templateId.matches(POKEMON_TEMPLATE_PATTERN)) {
			return new PokemonTemplate(json);
		} else if (templateId.matches(MOVE_TEMPLATE_PATTERN)) {
			return new MoveTemplate(json);
		} else {
			return null;
		}
	}
}
