package fi.iki.asb.pogo.pokedex;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class PokemonTemplate extends Template {

	private final JsonObject settings;
	
	PokemonTemplate(JsonObject json) {
		super(Template.Type.POKEMON, json);
		settings = json.get("pokemonSettings").getAsJsonObject();
	}

	private String id = null;
	public String getId() {
		if (id == null) {
			id = settings.get("pokemonId").getAsString();
		}
		return id;
	}

	private String name = null;
	public String getName() {
		if (name == null) {
			name = constructName(getId(), getForm());
		}

		return name;
	}

	public String getForm() {
		if (settings.get("form") == null) {
			return null;
		} else {
			return settings.get("form").getAsString();
		}
	}

	public int getSta() {
		return getStat("baseStamina");
	}

	public int getAtt() {
		return getStat("baseAttack");
	}

	public int getDef() {
		return getStat("baseDefense");
	}
	
	private List<String> evolutions = null;
	public List<String> getEvolutions() {
		if (evolutions == null) {
			evolutions = new ArrayList<>();
			JsonArray branches = settings.getAsJsonArray("evolutionBranch");
			if (branches == null) {
				return new ArrayList<>();
			}

			for (JsonElement e: branches) {
				JsonObject o = e.getAsJsonObject();
				String id = o.get("evolution").getAsString();
				String form = o.get("form") != null ? o.get("form").getAsString() : null;
				evolutions.add(constructName(id, form));
			}
		}

		return evolutions;
	}
	
	// ============================================================= //

	private int getStat(String val) {
		return settings
				.get("stats")
				.getAsJsonObject()
				.get(val)
				.getAsInt();
	}

	private String constructName(String id, String form) {
		if (form == null || form.endsWith("_NORMAL")) {
			return getNormalizedId(id);
		} else {
			return getNormalizedId(id) + " (" + getNormalizedForm(id, form) + ")";
		}
	}

	private String getNormalizedId(String id) {
		if (id.equals("MR_MIME")) {
			return "MR. MIME";
		} else if (id.equals("PORYGON2")) {
			return "PORYGON 2";
		} else if (id.equals("PORYGON_Z")) {
			return "PORYGON Z";
		} else {
			return id;
		}
	}

	private String getNormalizedForm(String id, String form) {
		if (form == null) {
			return null;
		}
		
		form = form.substring(id.length() + 1)
				.replace('_', ' ')
				.trim();

		if (form.equals("ALOLA"))
			return "ALOLAN";
		else
			return form;
	}

	public List<String> getFastMoves() {
		List<String> moves = new ArrayList<>();
		for (JsonElement e: settings.get("quickMoves").getAsJsonArray()) {
			moves.add(e.getAsString());
		}
		return moves;
	}

	public List<String> getChargeMoves() {
		List<String> moves = new ArrayList<>();
		for (JsonElement e: settings.get("cinematicMoves").getAsJsonArray()) {
			moves.add(e.getAsString());
		}
		return moves;
	}
	
	public List<String> getTypes() {
		List<String> types = new ArrayList<>();

		types.add(settings.get("type").getAsString());
		if (settings.get("type2") != null) {
			types.add(settings.get("type2").getAsString());
		}

		return types;
	}

}
