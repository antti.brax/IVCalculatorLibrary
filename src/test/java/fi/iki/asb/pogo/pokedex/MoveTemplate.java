package fi.iki.asb.pogo.pokedex;

import com.google.gson.JsonObject;

public class MoveTemplate extends Template {
	
	private JsonObject settings;

	public MoveTemplate(JsonObject json) {
		super(Template.Type.MOVE, json);
		settings = json.get("moveSettings").getAsJsonObject();
	}

	public String getId() {
		return settings.get("movementId").getAsString();
	}
	
	public String getName() {
		String id = getId();
		if (id.endsWith("_BLASTOISE")) {
			id = id.substring(0,  id.length() - 10);
		}

		if (id.endsWith("_FAST")) {
			id = id.substring(0,  id.length() - 5);
		}

		return id.replace('_', ' ');
	}
	
	public Double getPower() {
		if (settings.get("power") == null) {
			return 0.0;
		}

		return settings.get("power").getAsDouble();
	}

	public int getDuration() {
		return settings.get("durationMs").getAsInt();
	}

	public int getEnergyDelta() {
		if (settings.get("energyDelta") == null) {
			return 0;
		}

		return settings.get("energyDelta").getAsInt();
	}

	public String getType() {
		return settings.get("pokemonType").getAsString();
	}
}
