package fi.iki.asb.pogo.pokedex;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;

import fi.iki.asb.pogo.pokedex.Template.Type;

public class GameMasterParser {
	
	private static final String IN_FILE = "src/test/resources/fi/iki/asb/pogo/tool/GAME_MASTER.json";

	private static final String POKEDEX_OUT_FILE = "src/main/resources/fi/iki/asb/pogo/pokedex.json";
	private static final String MOVES_OUT_FILE = "src/main/resources/fi/iki/asb/pogo/moves.json";

	public static void main(String[] args) throws JsonIOException, IOException {
		final Gson gson = new Gson();
		final JsonObject doc = gson.fromJson(getReader(), JsonObject.class);

		List<PokemonSpecies> speciesList = new ArrayList<>(1000);
		List<Move> movesList = new ArrayList<>(1000);

		HashMap<String, PokemonSpecies> speciesMap = new HashMap<>();

		for (JsonElement templateElement: doc.getAsJsonArray("itemTemplates")) {
			Template template = TemplateFactory.getTemplate(templateElement.getAsJsonObject());
			if (template == null) {
				continue;
			}
			
			if (template.getTemplateType() == Type.POKEMON) {
				PokemonTemplate pt = (PokemonTemplate) template;

				pt.getEvolutions();
				Stats stats = new Stats(
						pt.getSta(),
						pt.getAtt(),
						pt.getDef());

				PokemonSpecies species = new PokemonSpecies(
						pt.getNumber(),
						pt.getName(),
						stats,
						pt.getEvolutions(),
						null,
						pt.getFastMoves(),
						pt.getChargeMoves(),
						pt.getTypes());

				speciesList.add(species);
				speciesMap.put(species.getName(), species);
			} else if (template.getTemplateType() == Type.MOVE) {
				MoveTemplate mt = (MoveTemplate) template;
				Move move = new Move(
						mt.getId(),
						mt.getName(),
						mt.getPower(),
						mt.getDuration(),
						mt.getEnergyDelta(),
						mt.getType());
				movesList.add(move);
			}
		}

		for (PokemonSpecies s: speciesList) {
			for (String evolvedName: s.getEvolutions()) {
				speciesMap.get(evolvedName).setPredecessor(s.getName());
			}
		}

		Writer writer = getWriter(POKEDEX_OUT_FILE);
		gson.toJson(speciesList, writer);
		writer.flush();
		writer.close();

		writer = getWriter(MOVES_OUT_FILE);
		gson.toJson(movesList, writer);
		writer.flush();
		writer.close();
	}

	private static Reader getReader() throws IOException {
		return new InputStreamReader(
				new FileInputStream(IN_FILE),
				"UTF-8");
	}

	private static Writer getWriter(String file) throws IOException {
		return new OutputStreamWriter(
				new FileOutputStream(file),
				"UTF-8");
	}
	

}
