package fi.iki.asb.pogo.pokedex;

import com.google.gson.JsonObject;

public abstract class Template {
	
	public static final String TEMPLATE_ID = "templateId";
	
	public enum Type {
		POKEMON,
		MOVE,
	}
	
	private final Type type;
	protected final JsonObject json;

	protected Template(Type type, JsonObject json) {
		super();
		this.type = type;
		this.json = json;
	}

	public Type getTemplateType() {
		return type;
	}

	private Integer number = null;
	public int getNumber() {
		if (number == null) {
			String templateId = json.get(TEMPLATE_ID).getAsString();
			number = Integer.parseInt(templateId.substring(1, 5));
		}
		return number;
	}
}
