package fi.iki.asb.pogo.pokemon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import org.junit.Test;

import fi.iki.asb.pogo.TestBase;
import fi.iki.asb.pogo.calculator.Levels;

public class PokemonTest extends TestBase {

	@Test
	public void initShouldMakeDeepCopy() {
		Pokemon original = Pokemon.ofSpecies(pokedex.findByName("pidgey"));
		Pokemon copy = Pokemon.from(original);

		assertNotSame(original, copy);
		assertNotSame(original.getAppraisal(), copy.getAppraisal());
		assertNotSame(original.getIndividualValues(), copy.getIndividualValues());

		assertSame(original.getLevel(), copy.getLevel());
		assertSame(original.getSpecies(), copy.getSpecies());

		assertEquals(original.getCp(), copy.getCp());
		assertEquals(original.getHp(), copy.getHp());
		assertEquals(original.getPowerUpCost(), copy.getPowerUpCost());
	}

	@Test
	public void shouldEvolveWithSameStats() {
		Pokemon original = Pokemon.ofSpecies(pokedex.findByName("pidgey"));
		original.setCp(123);
		original.setHp(45);
		original.setLevel(Levels.instance().getLevel(5));
		original.getIndividualValues().setStamina(11);
		original.getIndividualValues().setAttack(12);
		original.getIndividualValues().setDefense(13);

		Pokemon evolved = original.evolve(pokedex.findByName("pidgeotto"));

		assertSame(original.getLevel(), evolved.getLevel());
		assertNull(evolved.getCp());
		assertNull(evolved.getHp());
		assertEquals(Integer.valueOf(11), evolved.getIndividualValues().getStamina());
		assertEquals(Integer.valueOf(12), evolved.getIndividualValues().getAttack());
		assertEquals(Integer.valueOf(13), evolved.getIndividualValues().getDefense());
	}

	@Test
	public void shouldDevolve() {
		Pokemon original = Pokemon.ofSpecies(pokedex.findByName("pidgeotto"));
		original.evolve(pokedex.findByName("pidgey"));
	}

	@Test(expected=IllegalArgumentException.class)
	public void shouldNotEvolveToWrongSpecies() {
		Pokemon original = Pokemon.ofSpecies(pokedex.findByName("mr. mime"));
		original.evolve(pokedex.findByName("ditto"));
	}
}
