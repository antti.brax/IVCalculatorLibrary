package fi.iki.asb.pogo.pokemon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AppraisalTest {

	@Test
	public void test100() {

		Appraisal app = Appraisal.unknown();
		app.setStaminaBest(true);
		app.setAttackBest(true);
		app.setDefenseBest(true);
		
		IndividualValues ivs = IndividualValues.unknown();
		ivs.setStamina(15);
		ivs.setAttack(15);
		ivs.setDefense(15);
		
		assertTrue(app.matches(ivs));
	}

	@Test
	public void initShouldMakeDeepCopy() {
		Appraisal original = Appraisal.unknown();
		Appraisal copy = Appraisal.from(original);

		assertNotSame(original, copy);
		assertEquals(original.getPerfectionTier(), copy.getPerfectionTier());
		assertEquals(original.getTopStatTier(), copy.getTopStatTier());
		assertEquals(original.isAttackBest(), copy.isAttackBest());
		assertEquals(original.isDefenseBest(), copy.isDefenseBest());
		assertEquals(original.isStaminaBest(), copy.isStaminaBest());
	}


	@Test
	public void testIssue1() {

		Appraisal app = Appraisal.unknown();
		app.setStaminaBest(true);
		app.setAttackBest(true);
		app.setDefenseBest(true);
		
		IndividualValues ivs = IndividualValues.unknown();
		ivs.setStamina(12);
		ivs.setAttack(9);
		ivs.setDefense(15);
		
		assertFalse(app.matches(ivs));
	}

}
