package fi.iki.asb.pogo.calculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fi.iki.asb.pogo.TestBase;
import fi.iki.asb.pogo.pokemon.Pokemon;
import fi.iki.asb.pogo.pokemon.PoweredUp;
import fi.iki.asb.pogo.pokemon.Appraisal.PerfectionTier;
import fi.iki.asb.pogo.pokemon.Appraisal.TopStatTier;

public class ChikoritaTest extends TestBase {
	
	@Test
	public void shouldEvolve() {
		Pokemon pokemon = Pokemon.ofSpecies(pokedex.findByName("Chikorita"));
		pokemon.setCp(682);
		pokemon.setHp(94);
		pokemon.setPoweredUp(PoweredUp.NO);
		pokemon.setPowerUpCost(4500);
		pokemon.getAppraisal().setPerfectionTier(PerfectionTier.TIER_2);
		pokemon.getAppraisal().setAttackBest(true);
		pokemon.getAppraisal().setTopStatTier(TopStatTier.TIER_1);
		
		pokemon = calculator.calculateIV(pokemon).get(0);
		assertIvs(pokemon, 8, 15, 7);
		

		Pokemon evolved = pokemon.evolve(pokedex.findByName("Bayleef"));
		assertTrue(calculator.calculateCp(evolved));
		assertEquals(Integer.valueOf(1072), evolved.getCp());

		assertTrue(calculator.calculateHp(evolved));
		assertEquals(Integer.valueOf(113), evolved.getHp());
	}

}
