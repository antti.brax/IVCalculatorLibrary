package fi.iki.asb.pogo.calculator;

import org.junit.Test;

import fi.iki.asb.pogo.TestBase;
import fi.iki.asb.pogo.pokedex.PokemonSpecies;
import fi.iki.asb.pogo.pokemon.Appraisal.PerfectionTier;
import fi.iki.asb.pogo.pokemon.Appraisal.TopStatTier;
import fi.iki.asb.pogo.pokemon.Pokemon;
import fi.iki.asb.pogo.pokemon.PoweredUp;

public class DragoniteTest extends TestBase {

	@Test
	public void testMetagross1() {
		PokemonSpecies species = pokedex.findByName("Metagross");
		Pokemon pokemon = Pokemon.ofSpecies(species);

		pokemon.setCp(3338);
		pokemon.setHp(150);
		pokemon.setPowerUpCost(6000);
		pokemon.setPoweredUp(PoweredUp.MAYBE);
		pokemon.getAppraisal().setAttackBest(true);
		pokemon.getAppraisal().setTopStatTier(TopStatTier.TIER_1);
		pokemon.getAppraisal().setPerfectionTier(PerfectionTier.TIER_1);

		Pokemon result = calculator.calculateIV(pokemon).get(0);
		assertIvs(result, 11, 15, 13);
	}

	@Test
	public void testMetagross2() {
		PokemonSpecies species = pokedex.findByName("Metagross");
		Pokemon pokemon = Pokemon.ofSpecies(species);

		pokemon.setCp(2960);
		pokemon.setHp(147);
		pokemon.setPowerUpCost(5000);
		pokemon.setPoweredUp(PoweredUp.MAYBE);
		pokemon.getAppraisal().setStaminaBest(true);
		pokemon.getAppraisal().setTopStatTier(TopStatTier.TIER_1);
		pokemon.getAppraisal().setPerfectionTier(PerfectionTier.TIER_3);

		Pokemon result = calculator.calculateIV(pokemon).get(0);
		assertIvs(result, 15, 2, 10);
	}
}
