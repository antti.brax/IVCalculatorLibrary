package fi.iki.asb.pogo.calculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.List;

import org.junit.Test;

import fi.iki.asb.pogo.TestBase;
import fi.iki.asb.pogo.pokemon.Appraisal.PerfectionTier;
import fi.iki.asb.pogo.pokemon.Appraisal.TopStatTier;
import fi.iki.asb.pogo.pokemon.Pokemon;
import fi.iki.asb.pogo.pokemon.PoweredUp;

public class CalculatorTest extends TestBase {

	@Test
	public void shouldGetPossibleLevels() {
		Pokemon p = Pokemon.ofSpecies(pokedex.findByName("Rattata"));
		p.setPowerUpCost(1300);

		Collection<Level> levels = calculator.getPossibleLevels(p);
		assertEquals(4, levels.size());
		
		for (Level level: levels) {
			assertEquals(1300, level.getPowerUpCost(p.isLucky()));
		}
	}

	@Test
	public void shouldCalculateHp() {
		Pokemon pokemon = Pokemon.ofSpecies(pokedex.findByName("entei"));
		pokemon.setLevel(levels.getLevel(15));
		pokemon.getIndividualValues().setStamina(11);

		assertTrue(calculator.calculateHp(pokemon));
		assertEquals(Integer.valueOf(135), pokemon.getHp());
	}

	@Test
	public void shouldNotCalculateHp() {
		Pokemon pokemon = Pokemon.ofSpecies(pokedex.findByName("entei"));
		pokemon.setLevel(levels.getLevel(15));

		assertFalse(calculator.calculateHp(pokemon));
	}

	@Test
	public void shouldCalculateCp() {
		Pokemon pokemon = Pokemon.ofSpecies(pokedex.findByName("raikou"));

		pokemon.setLevel(levels.getLevel(15));
		pokemon.getIndividualValues().setSAD(10, 13, 14);

		assertTrue(calculator.calculateCp(pokemon));
		assertEquals(Integer.valueOf(1448), pokemon.getCp());
	}

	@Test
	public void shouldCalculateWithoutLevel() {
		Pokemon pokemon = Pokemon.ofSpecies(pokedex.findByName("wailord"));
		pokemon.setCp(1571);
		pokemon.setHp(241);

		List<Pokemon> results = calculator.calculateIV(pokemon);
		assertEquals(11,  results.size());

		assertIvs(results.get(0), 15, 15, 8);
		assertLevel(results.get(0), "25");

		assertIvs(results.get(7), 4, 12, 3);
		assertLevel(results.get(7), "26.5");

		assertIvs(results.get(10), 4, 6, 9);
		assertLevel(results.get(10), "26.5");
	}
	
	@Test
	public void shouldDiscardHalfLevelsIfNotPowered() {
		Pokemon pokemon = Pokemon.ofSpecies(pokedex.findByName("wailord"));
		pokemon.setCp(1571);
		pokemon.setHp(241);
		pokemon.setPoweredUp(PoweredUp.NO);

		List<Pokemon> results = calculator.calculateIV(pokemon);
		
		for (Pokemon p: results) {
			if (p.getLevel().isHalfLevel()) {
				fail(p.toString());
			}
		}

		assertEquals(6, results.size());
	}

	@Test
	public void shouldNotOverwriteIvs() {
		Pokemon pokemon = pokemon("metagross");
		pokemon.setCp(3391);
		pokemon.setHp(151);
		pokemon.getIndividualValues().setAttack(15);

		// If the preset attack was not considered, this would result in
		// 20 matches.
		List<Pokemon> results = calculator.calculateIV(pokemon);
		assertEquals(1, results.size());
		assertIvs(results.get(0), 11, 15, 13);
	}

	@Test
	public void shouldCalculateAppraisal() {
		Pokemon pokemon = pokemon("pidgey");
		pokemon.getIndividualValues().setSAD(0, 0, 0);

		assertTrue(calculator.calculateAppraisal(pokemon));
		assertTrue(pokemon.getAppraisal().isStaminaBest());
		assertTrue(pokemon.getAppraisal().isAttackBest());
		assertTrue(pokemon.getAppraisal().isDefenseBest());
		assertEquals(TopStatTier.TIER_4, pokemon.getAppraisal().getTopStatTier());
		assertEquals(PerfectionTier.TIER_4, pokemon.getAppraisal().getPerfectionTier());

		pokemon.getIndividualValues().setSAD(1, 0, 0);
		assertTrue(calculator.calculateAppraisal(pokemon));
		assertTrue(pokemon.getAppraisal().isStaminaBest());
		assertFalse(pokemon.getAppraisal().isAttackBest());
		assertFalse(pokemon.getAppraisal().isDefenseBest());
		assertEquals(TopStatTier.TIER_4, pokemon.getAppraisal().getTopStatTier());
		assertEquals(PerfectionTier.TIER_4, pokemon.getAppraisal().getPerfectionTier());

		pokemon.getIndividualValues().setSAD(15, 14, 15);
		assertTrue(calculator.calculateAppraisal(pokemon));
		assertTrue(pokemon.getAppraisal().isStaminaBest());
		assertFalse(pokemon.getAppraisal().isAttackBest());
		assertTrue(pokemon.getAppraisal().isDefenseBest());
		assertEquals(TopStatTier.TIER_1, pokemon.getAppraisal().getTopStatTier());
		assertEquals(PerfectionTier.TIER_1, pokemon.getAppraisal().getPerfectionTier());
	}

	@Test
	public void shouldNotCalculateAppraisal() {
		Pokemon pokemon = pokemon("pidgey");
		pokemon.getIndividualValues().setSAD(null, 0, 0);
		
		assertFalse(calculator.calculateAppraisal(pokemon));
		assertEquals(PerfectionTier.UNKNOWN, pokemon.getAppraisal().getPerfectionTier());
		assertEquals(TopStatTier.UNKNOWN, pokemon.getAppraisal().getTopStatTier());
	}
}
