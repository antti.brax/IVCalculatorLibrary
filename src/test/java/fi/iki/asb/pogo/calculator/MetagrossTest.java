package fi.iki.asb.pogo.calculator;

import org.junit.Test;

import fi.iki.asb.pogo.TestBase;
import fi.iki.asb.pogo.pokedex.PokemonSpecies;
import fi.iki.asb.pogo.pokemon.Appraisal.PerfectionTier;
import fi.iki.asb.pogo.pokemon.Appraisal.TopStatTier;
import fi.iki.asb.pogo.pokemon.Pokemon;
import fi.iki.asb.pogo.pokemon.PoweredUp;

public class MetagrossTest extends TestBase {

	@Test
	public void testDragonite() {
		PokemonSpecies species = pokedex.findByName("Dragonite");
		Pokemon pokemon = Pokemon.ofSpecies(species);

		pokemon.setCp(3374);
		pokemon.setHp(164);
		pokemon.setPowerUpCost(7000);
		pokemon.setPoweredUp(PoweredUp.YES);
		pokemon.getAppraisal().setAttackBest(true);
		pokemon.getAppraisal().setDefenseBest(true);
		pokemon.getAppraisal().setTopStatTier(TopStatTier.TIER_2);
		pokemon.getAppraisal().setPerfectionTier(PerfectionTier.TIER_1);

		Pokemon result = calculator.calculateIV(pokemon).get(0);
		assertIvs(result, 9, 14, 14);
	}
}
