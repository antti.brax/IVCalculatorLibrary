package fi.iki.asb.pogo.calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fi.iki.asb.pogo.TestBase;
import fi.iki.asb.pogo.pokedex.PokemonSpecies;
import fi.iki.asb.pogo.pokemon.Appraisal.PerfectionTier;
import fi.iki.asb.pogo.pokemon.Appraisal.TopStatTier;
import fi.iki.asb.pogo.pokemon.Pokemon;
import fi.iki.asb.pogo.pokemon.PoweredUp;

public class WeavileTest extends TestBase {

	@Test
	public void test() {
		PokemonSpecies species = pokedex.findByName("Weavile");
		Pokemon pokemon = Pokemon.ofSpecies(species);

		pokemon.setCp(1689);
		pokemon.setHp(109);
		pokemon.setPowerUpCost(2500);
		pokemon.setPoweredUp(PoweredUp.NO);
		pokemon.getAppraisal().setAttackBest(true);
		pokemon.getAppraisal().setTopStatTier(TopStatTier.TIER_1);
		pokemon.getAppraisal().setPerfectionTier(PerfectionTier.TIER_1);

		assertEquals(2, calculator.calculateIV(pokemon).size());
		assertIvs(calculator.calculateIV(pokemon).get(0), 11, 15, 13);
		assertIvs(calculator.calculateIV(pokemon).get(1), 12, 15, 12);
	}
}
