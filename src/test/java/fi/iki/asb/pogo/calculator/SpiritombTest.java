package fi.iki.asb.pogo.calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fi.iki.asb.pogo.TestBase;
import fi.iki.asb.pogo.pokedex.PokemonSpecies;
import fi.iki.asb.pogo.pokemon.Appraisal.PerfectionTier;
import fi.iki.asb.pogo.pokemon.Appraisal.TopStatTier;
import fi.iki.asb.pogo.pokemon.Pokemon;
import fi.iki.asb.pogo.pokemon.PoweredUp;

public class SpiritombTest extends TestBase {

	@Test
	public void test() {
		PokemonSpecies species = pokedex.findByName("Spiritomb");
		Pokemon pokemon = Pokemon.ofSpecies(species);

		pokemon.setCp(874);
		pokemon.setHp(78);
		pokemon.setPowerUpCost(1900);
		pokemon.setPoweredUp(PoweredUp.NO);
		pokemon.getAppraisal().setStaminaBest(true);
		pokemon.getAppraisal().setTopStatTier(TopStatTier.TIER_1);
		pokemon.getAppraisal().setPerfectionTier(PerfectionTier.TIER_1);

		assertEquals(1, calculator.calculateIV(pokemon).size());
		assertIvs(calculator.calculateIV(pokemon).get(0), 15, 13, 13);
	}
}
