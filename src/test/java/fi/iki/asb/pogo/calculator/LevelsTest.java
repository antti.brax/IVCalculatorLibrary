package fi.iki.asb.pogo.calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fi.iki.asb.pogo.TestBase;
import fi.iki.asb.pogo.pokemon.Lucky;
import fi.iki.asb.pogo.pokemon.PoweredUp;

public class LevelsTest extends TestBase {

	@Test
	public void test() {
		assertEquals("30", levels.getLevel(30).getName());
	}
	
	@Test
	public void shouldReturnMaxLevel() {
		assertEquals("20", levels.getMaxLevel(2500, PoweredUp.NO, Lucky.NO).getName());
	}
}
