package fi.iki.asb.pogo.calculator;

import org.junit.Test;

import fi.iki.asb.pogo.TestBase;
import fi.iki.asb.pogo.pokedex.PokemonSpecies;
import fi.iki.asb.pogo.pokemon.Appraisal.PerfectionTier;
import fi.iki.asb.pogo.pokemon.Pokemon;
import fi.iki.asb.pogo.pokemon.PoweredUp;

public class BlisseyTest extends TestBase {

	@Test
	public void testBlissey() {
		PokemonSpecies species = pokedex.findByName("Blissey");
		Pokemon pokemon = Pokemon.ofSpecies(species);

		pokemon.setCp(2245);
		pokemon.setHp(364);
		pokemon.setPowerUpCost(4500);
		pokemon.setPoweredUp(PoweredUp.YES);
		pokemon.getAppraisal().setPerfectionTier(PerfectionTier.TIER_1);

		Pokemon result = calculator.calculateIV(pokemon).get(0);
		assertIvs(result, 15, 15, 15);
	}
}
