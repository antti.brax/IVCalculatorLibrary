package fi.iki.asb.pogo;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;

import fi.iki.asb.pogo.calculator.Calculator;
import fi.iki.asb.pogo.calculator.MoveCalculator;
import fi.iki.asb.pogo.calculator.Levels;
import fi.iki.asb.pogo.pokedex.Moves;
import fi.iki.asb.pogo.pokedex.Pokedex;
import fi.iki.asb.pogo.pokemon.IndividualValues;
import fi.iki.asb.pogo.pokemon.Pokemon;

public abstract class TestBase {

	protected Pokedex pokedex;
	protected Calculator calculator;
	protected MoveCalculator dpsCalculator;
	protected Levels levels;
	protected Moves moves;

	@Before
	public void setUp() {
		pokedex = Pokedex.instance();
		calculator = Calculator.instance();
		dpsCalculator = MoveCalculator.instance();
		levels = Levels.instance();
		moves = Moves.instance();
	}

	@After
	public void tearDown() {
		pokedex = null;
		calculator = null;
		levels = null;
	}
	
	protected Pokemon pokemon(String name) {
		return Pokemon.ofSpecies(pokedex.findByName(name));
	}

	protected static void assertIvs(Pokemon pokemon, int sta, int att, int def) {
		IndividualValues ivs = pokemon.getIndividualValues();
		
		String pattern = "%d, %d, %d";

		assertEquals(String.format(pattern, sta, att, def), String.format(pattern, ivs.getStamina(), ivs.getAttack(), ivs.getDefense()));
	}

	protected static void assertLevel(Pokemon pokemon, String level) {
		assertEquals(level, pokemon.getLevel().getName());
	}
}
